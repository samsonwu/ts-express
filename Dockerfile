FROM node:lts-alpine3.11

RUN apk update \
  && apk add ca-certificates make

RUN npm i -g pm2

ARG env

ENV ROOT_APP_DIR=/webapp/current

WORKDIR $ROOT_APP_DIR

COPY ./environments/${env}.env $ROOT_APP_DIR/.env
COPY ./package*.json ./pm2.json $ROOT_APP_DIR/

RUN yarn install

COPY ./output $ROOT_APP_DIR/output

EXPOSE 7000

CMD ["pm2-runtime", "pm2.json"]
