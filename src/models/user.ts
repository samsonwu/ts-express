import { FilterQuery } from 'mongodb'
import { EditableMongoAccessor, IEditableRecord } from '../libs/editable-mongo-accessor'

export interface IUser extends IEditableRecord {
  nickname?: string
  avatar?: string
  username?: string
  password?: string
  mobile?: string
  email?: string
}

export class User extends EditableMongoAccessor<IUser> {
  protected collectionName: string = 'user'
  private defaultProjection: any = {
    _id: 0,
    id: 1,
    nickname: 1,
    avatar: 1,
  }

  public getUser(userId: string) {
    return this.get(
      {
        id: userId,
      },
      { projection: this.defaultProjection },
    )
  }

  public getUsers(userIds: string[]) {
    return this.get(
      {
        id: {
          $in: userIds,
        },
      },
      { projection: this.defaultProjection },
    )
  }

  public getFullUser(query: FilterQuery<IUser>) {
    return this.get(query, {
      projection: {
        _id: 0,
        id: 1,
        nickname: 1,
        avatar: 1,
        username: 1,
        password: 1,
        roles: 1,
        mobile: 1,
        email: 1,
      },
    })
  }
}
