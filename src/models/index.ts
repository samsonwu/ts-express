import { mongoConfig } from '../configs'
import { User } from './../models/user'

export class Models {
  public static get userModel() {
    this.user = this.user || new User(mongoConfig)
    return this.user
  }

  private static user: User
}
