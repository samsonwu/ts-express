import { expect } from 'chai'
import { createRequest, createResponse } from 'node-mocks-http'

import { createAppLogger } from './create-logger'

describe('createLogger', () => {
  it('should create logger and record request via log info', async () => {
    const req = createRequest({
      query: { nothing: 'nothing' },
    })
    const res = createResponse()
    await createAppLogger(req as any, res, () => {})
    expect(req.logger).not.null
  })
})
