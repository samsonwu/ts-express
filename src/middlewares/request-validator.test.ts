import { expect } from 'chai'
import * as Joi from '@hapi/joi'
import { spy } from 'sinon'

import { IRequestValidatorSchema, requestValidator } from './request-validator'
import { createRequest, createResponse } from 'node-mocks-http'

describe('requestValidator', () => {
  it.only('should use joi to validate', async () => {
    const req = createRequest({
      query: { nothing: 'nothing' },
    })
    const res = createResponse()

    const schemas: IRequestValidatorSchema = {
      query: Joi.object().keys({
        nothing: Joi.string().optional(),
      }),
    }

    const validateSpy = spy(schemas.query, 'validate')
    const nextSpy = spy(() => {})

    await requestValidator(schemas)(req as any, res, nextSpy)
    expect(validateSpy.calledOnce).to.true
    expect(nextSpy.calledOnce).to.true
  })
})
