import { Response, NextFunction } from 'express'

import { isBoom } from '@hapi/boom'
import { ICustomRequest } from './create-logger'
import { isProd } from '../configs/index'

export const handleErrors = (err, req: ICustomRequest, res: Response, next: NextFunction) => {
  req.logger.error(err)

  if (isBoom(err)) {
    res.status(err.output.statusCode).send(err.output.payload.message)
  } else {
    let unexpectedErrorMsg = 'Internal server errors.'

    if (!isProd()) {
      unexpectedErrorMsg = `error: ${err.message}, stack: ${err.stack}`
    }

    res.status(err.statusCode || 500).send(unexpectedErrorMsg)
  }
  next()
}

export const catchErrors = (middleware: (req: ICustomRequest, res: Response) => Promise<void>) => async (
  req: ICustomRequest,
  res: Response,
  next: NextFunction,
) => {
  try {
    await middleware(req, res)
  } catch (error) {
    next(error)
  }
}
