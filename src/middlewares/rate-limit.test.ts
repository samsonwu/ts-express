import { expect } from 'chai'

import { rateLimitOptions } from '../configs/index'
import { rateLimit } from './rate-limit'
import { createRequest, createResponse } from 'node-mocks-http'

describe('rateLimiting', () => {
  it('should set limit if requests threshold reached', async () => {
    rateLimitOptions.limitedRequests = 5

    const req = createRequest({
      query: { nothing: 'nothing' },
      headers: { 'x-forwarded-for': '172.0.0.1' },
    })
    const res = createResponse()

    let limit = 0
    try {
      for (let requests = 0; requests <= rateLimitOptions.limitedRequests; requests++) {
        await rateLimit(req as any, res, () => {})
      }
    } catch (error) {
      expect(error.isBoom).to.true
      expect(error.message).to.equals('processing, please try it later.')
    }
  })
})
