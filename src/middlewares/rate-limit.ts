import { Response, NextFunction } from 'express'
import Boom = require('@hapi/boom')
import _ = require('lodash')
import Lru = require('lru-cache')
import uuid = require('uuid/v4')

import { rateLimitOptions } from '../configs/index'
import { ICustomRequest } from './create-logger'

const cache = new Lru<string, number>({
  max: rateLimitOptions.max,
  maxAge: rateLimitOptions.maxAge,
  updateAgeOnGet: true,
})

export const rateLimit = async (req: ICustomRequest, res: Response, next: NextFunction) => {
  if (req.url !== '/auth') {
    // Only works if your reverse proxy assigned X-Forwarded-For as below:
    // proxy_set_header X-Forwarded-For $remote_addr;
    const ip = req.headers['x-forwarded-for'] || uuid()
    // ip is a temp solution, it should sign a unique token (expireable) to app clients.
    const identity = ip + req.url
    const limit = cache.get(identity)
    if (_.isNil(limit)) {
      cache.set(identity, 1)
    } else {
      // the configuration can be extended to use a map of each route
      // with difference number of limited requests
      if (limit >= rateLimitOptions.limitedRequests) {
        throw Boom.tooManyRequests('processing, please try it later.')
      }
      cache.set(identity, limit + 1)
    }
  }
  await next()
}
