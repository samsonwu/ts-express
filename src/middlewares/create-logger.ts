import { Request, Response, NextFunction } from 'express'

import * as _ from 'lodash'
import * as uuid from 'uuid/v4'

import { Logger } from '../libs/index'
import { logLevel, logTransports, isProd } from '../configs/index'
import morgan = require('morgan')

export interface ICustomRequest extends Request {
  logger: Logger
  props: any
}

export const createRequestLogger = (req: ICustomRequest, res: Response, next: NextFunction) => {
  req.logger = new Logger('my-app', uuid(), logLevel, logTransports)
  next()
}

const morganFormat =
  ':remote-addr - :remote-user ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent"'

export const logRequest = (req: ICustomRequest, res: Response, next: NextFunction) => {
  // Express morgan to format the request as the common way of express
  morgan(morganFormat, {
    immediate: true,
    stream: {
      write: (msg) => req.logger.info(msg),
    },
  })(req, res, next)

  next()
}
