import { Response, NextFunction } from 'express'

import * as Boom from '@hapi/boom'
import * as Joi from '@hapi/joi'
import * as _ from 'lodash'

import { ICustomRequest } from './create-logger'

export interface IRequestValidatorSchema {
  header?: Joi.AnySchema
  body?: Joi.AnySchema
  query?: Joi.AnySchema
  params?: Joi.AnySchema
}

export const requestValidator = (schemas: IRequestValidatorSchema, options: Joi.ValidationOptions = {}) => async (
  req: ICustomRequest,
  res: Response,
  next: NextFunction,
) => {
  try {
    validateIfNotNull(req.header, schemas.header, options)
    validateIfNotNull(req.params, schemas.params, options)
    validateIfNotNull(req.query, schemas.query, options)
    validateIfNotNull(req.body, schemas.body, options, true)
  } catch (err) {
    throw Boom.badRequest(`validation failed on: ${err.message}`)
  }
  await next()
}

const validateIfNotNull = (
  data: any,
  schema: Joi.AnySchema,
  options: Joi.ValidationOptions = {},
  ignoreNil: boolean = false,
) => {
  options.convert = true
  options.stripUnknown = true
  if (data && schema) {
    if (ignoreNil) {
      const trimmedData = cleanNested(data)
      return schema.validate(trimmedData, options)
    }
    return schema.validate(data, options)
  }
  return data
}

const cleanNested = (obj: any) => {
  if (_.isArray(obj)) {
    for (const item of obj) {
      cleanNested(item)
    }
  }
  _.forOwn(obj, function (value, key) {
    // We only treats empty string as null, but empty array.
    if (_.isNil(value) || value === '') {
      delete obj[key]
    }
    if (_.isArray(value) || _.isObject(value)) {
      cleanNested(value)
    }
  })
  return obj
}
