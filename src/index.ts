import express = require('express')

import { handleErrors } from './middlewares/catch-errors'
import { createRequestLogger, logRequest } from './middlewares/create-logger'
import { router } from './routes'

const app = express()
app.use(createRequestLogger)
app.use(logRequest)
app.use(router)
app.use((err, req, res, next) => {
  handleErrors(err, req, res, next)
})

const port = Number(process.env.PORT) || 3000

export const expressSvr = app.listen(port, () => console.log(`My App listening on port: ${port}`))
