require('dotenv').config()

import { IMongoOptions } from '../libs/mongo-accessor'

export const mongoConfig: IMongoOptions = {
  mongoHost: process.env.MONGO_CONFIG_HOST,
  mongoUsername: process.env.MONGO_CONFIG_USERNAME,
  mongoPassword: process.env.MONGO_CONFIG_PASSWORD,
  mongoPort: process.env.MONGO_CONFIG_PORT,
  mongoDbName: process.env.MONGO_INITDB_DATABASE,
  mongoUseDnsSeedlist: process.env.MONGO_USE_DNS_SEED_LIST === 'true',
  mongoOptions: {
    connectTimeoutMS: 20000,
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
}
