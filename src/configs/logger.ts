import { Transport, LogLevel } from '../libs/logger'
import { transports } from 'winston'

export const logLevel: LogLevel = (LogLevel as any)[process.env.LOG_LEVEL]

export const logTransports: Transport[] = []

if (['dev', 'test'].includes(process.env.ENV)) {
  logTransports.push(new transports.Console())
} else {
  logTransports.push(new transports.File({ filename: 'combined.log' }))
}
