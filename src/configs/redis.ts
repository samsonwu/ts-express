export const redisConfig: IRedisConfig = {
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT,
}

export interface IRedisConfig {
  host?: string
  port?: string
}
