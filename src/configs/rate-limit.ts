export const rateLimitOptions = {
  max: Number(process.env.RATE_LIMIT_MAX),
  maxAge: Number(process.env.RATE_LIMIT_MAX_MINS) * 1000 * 60,
  limitedRequests: Number(process.env.LIMITED_REQUESTS),
}
