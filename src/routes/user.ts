import { Router } from 'express'
import Joi = require('@hapi/joi')
import { requestValidator, IRequestValidatorSchema } from '../middlewares/request-validator'

import { UserController } from '../controllers/user-controller'
import { catchErrors } from '../middlewares/catch-errors'

export const userRouter = Router()

const getSchema: IRequestValidatorSchema = {
  params: Joi.object().keys({
    id: Joi.string(),
  }),
}

userRouter.get('/:id', requestValidator(getSchema), catchErrors(UserController.get))
