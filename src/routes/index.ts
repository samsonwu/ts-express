import { Router } from 'express'

import { healthRouter } from './health'
import { userRouter } from './user'

export const router = Router()

router.use('/health', healthRouter)
router.use('/user', userRouter)
