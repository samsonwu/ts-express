import { Router, RequestHandler } from 'express'

export const healthRouter = Router()

const healthCheck: RequestHandler = (req, res) => {
  res.status(200).send({ message: 'Health Check: application is up and running' })
}

healthRouter.get('/', healthCheck)
