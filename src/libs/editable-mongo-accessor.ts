import * as _ from 'lodash'
import { CollectionInsertOneOptions, FilterQuery, FindOneAndUpdateOption, ReplaceOneOptions } from 'mongodb'
import * as uuid from 'uuid'
import { IMongoOptions, MongoAccessor } from './mongo-accessor'

export abstract class EditableMongoAccessor<T extends IEditableRecord> extends MongoAccessor<T> {
  private operator: string

  constructor(mongoConfig: IMongoOptions) {
    super(mongoConfig)
  }

  public initOperator(operator: string) {
    this.operator = operator
  }

  public async insertOne(fullDoc: T, options?: CollectionInsertOneOptions) {
    if (_.isNil(fullDoc.createdBy) && _.isNil(this.operator)) {
      throw new Error('Missing createdBy!')
    }
    if (_.isNil(fullDoc.createdBy)) {
      fullDoc.createdBy = this.operator
    }
    if (_.isNil(fullDoc.id)) {
      fullDoc.id = uuid.v4()
    }
    fullDoc.deleted = false
    fullDoc.createdAt = new Date()
    return await super.insertOne(fullDoc, options)
  }

  public async insertOneBulk(fullDoc: T) {
    if (_.isNil(fullDoc.createdBy) && _.isNil(this.operator)) {
      throw new Error('Missing createdBy!')
    }
    if (_.isNil(fullDoc.createdBy)) {
      fullDoc.createdBy = this.operator
    }
    if (_.isNil(fullDoc.id)) {
      fullDoc.id = uuid.v4()
    }
    fullDoc.deleted = false
    fullDoc.createdAt = new Date()
    return await super.insertOneBulk(fullDoc)
  }

  public async updateOne(doc: T, query: FilterQuery<T>, options?: FindOneAndUpdateOption) {
    if (_.isNil(doc.updatedBy) && _.isNil(this.operator)) {
      throw new Error('Missing updatedBy!')
    }
    if (_.isNil(doc.updatedBy)) {
      doc.updatedBy = this.operator
    }
    doc.updatedAt = new Date()
    return await super.updateOne(doc, query, options)
  }

  public async updateOneBulk(doc: T, query: FilterQuery<T>) {
    if (_.isNil(doc.updatedBy) && _.isNil(this.operator)) {
      throw new Error('Missing updatedBy!')
    }
    if (_.isNil(doc.updatedBy)) {
      doc.updatedBy = this.operator
    }
    doc.updatedAt = new Date()
    return await super.updateOneBulk(doc, query)
  }

  public async softDeleteOne(doc: T, query: FilterQuery<T>, options?: ReplaceOneOptions) {
    if (_.isNil(doc.updatedBy) && _.isNil(this.operator)) {
      throw new Error('Missing updatedBy!')
    }
    if (_.isNil(doc.updatedBy)) {
      doc.updatedBy = this.operator
    }
    return await super.updateOne(
      {
        deleted: true,
        updatedAt: new Date(),
        updatedBy: doc.updatedBy,
      } as T,
      query,
      options,
    )
  }

  public async softDeleteOneBulk(doc: T, query: FilterQuery<T>) {
    if (_.isNil(doc.updatedBy) && _.isNil(this.operator)) {
      throw new Error('Missing updatedBy!')
    }
    if (_.isNil(doc.updatedBy)) {
      doc.updatedBy = this.operator
    }
    return await super.updateOneBulk(
      {
        deleted: true,
        updatedAt: new Date(),
        updatedBy: doc.updatedBy,
      } as T,
      query,
    )
  }
}

export interface IEditableRecord {
  id?: string
  deleted?: boolean
  createdAt?: Date
  createdBy?: string
  updatedAt?: Date
  updatedBy?: string
}
