import * as stream from 'stream'
import * as logform from 'logform'
import * as _ from 'lodash'
import * as uuid from 'uuid/v4'

import { createLogger, Logger as WinstonLogger, transports as winstonTransports, format } from 'winston'

export declare class Transport extends stream.Writable {
  public format?: logform.Format
  public level?: string
  public silent?: boolean
  public handleExceptions?: boolean

  constructor(opts?: TransportOptions)

  public log?(info: any, next: () => void): any
  public logv?(info: any, next: () => void): any
  public close?(): void
}

export interface TransportOptions {
  format?: logform.Format
  level?: string
  silent?: boolean
  handleExceptions?: boolean

  log?(info: any, next: () => void): any
  logv?(info: any, next: () => void): any
  close?(): void
}

export interface ILogConfig {
  transports: Transport[]
  level: LogLevel
}

export enum LogLevel {
  error = 'error',
  warn = 'warn',
  info = 'info',
  http = 'http',
  verbose = 'verbose',
  debug = 'debug',
  silly = 'silly',
}

export interface ILog {
  id: string
  app: string
  type?: LogLevel
  additionInfo?: any
  message?: string
  stack?: string
  createAt: Date
}

export const getLogger = (
  app: string = '',
  level: LogLevel = LogLevel.info,
  transports: Transport[] = [winstonTransports.Console],
  logFormat: logform.Format = defaultFormat,
  id: string = uuid(),
) => {
  return createLogger({
    level,
    transports,
    format: logFormat,
    defaultMeta: { service: app, requestId: id },
  })
}

const defaultFormat = format.combine(
  format.timestamp({
    format: 'YYYY-MM-DD HH:mm:ss',
  }),
  format.errors({ stack: true }),
  format.splat(),
  format.json(),
)

export class Logger {
  private static winstonLogger: WinstonLogger
  private level: LogLevel
  private transports: Transport[]
  private id: string
  private logFormat: logform.Format
  private app: string

  constructor(
    app: string = '',
    id: string = uuid(),
    level: LogLevel = LogLevel.info,
    transports: Transport[] = [winstonTransports.Console],
    logFormat: logform.Format = defaultFormat,
  ) {
    this.app = app
    this.id = id
    this.level = level
    this.transports = transports
    this.logFormat = logFormat
  }

  public resetLevel(newLevel: LogLevel) {
    this.getLogger().level = newLevel
  }

  public warn(message: string) {
    this.getLogger().warn({ id: this.id, message })
  }

  public error(error: Error) {
    this.getLogger().error(error)
  }

  public info(message: string) {
    this.getLogger().info({ id: this.id, message })
  }

  public debug(message: string) {
    this.getLogger().debug({ id: this.id, message })
  }

  public getLogger() {
    if (_.isNil(Logger.winstonLogger)) {
      for (const transport of this.transports) {
        transport.format = this.logFormat
      }
      Logger.winstonLogger = createLogger({
        level: this.level,
        transports: this.transports,
        defaultMeta: { service: this.app },
        exitOnError: false,
      })
    }
    return Logger.winstonLogger
  }
}
