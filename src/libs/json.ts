export class JSONHelper {
  public static safeStringify(value: any): string {
    let cache = []
    const valueStr = JSON.stringify(value, (_, val) => {
      if (typeof val === 'object' && val !== null) {
        if (cache.indexOf(val) !== -1) {
          return
        }
        cache.push(val)
      }
      return val
    })
    cache = null
    return valueStr
  }
}
