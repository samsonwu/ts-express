import * as _ from 'lodash'
import {
  CollectionAggregationOptions,
  CollectionInsertOneOptions,
  CommonOptions,
  Db,
  FilterQuery,
  FindOneAndDeleteOption,
  FindOneAndUpdateOption,
  FindOneOptions,
  MongoClient,
  MongoClientOptions,
  OrderedBulkOperation,
} from 'mongodb'

export interface IMongoOptions {
  mongoHost: string
  mongoUsername: string
  mongoPassword: string
  mongoPort: string
  mongoDbName: string
  mongoOptions?: MongoClientOptions
  mongoUseDnsSeedlist: boolean
  mongoConnectOptions?: string
}

export abstract class MongoAccessor<T extends object> {
  public static async close() {
    if (!_.isNil(MongoAccessor.mongoClient)) {
      const mongoClient = MongoAccessor.mongoClient
      await mongoClient.close()
      MongoAccessor.mongoClient = undefined
    }
  }
  private static mongoClient: MongoClient

  protected abstract collectionName: string

  private mongoConnection: string
  private dbName: string
  private mongoOptions: MongoClientOptions
  private db: Db
  private bulkOps: OrderedBulkOperation

  constructor(mongoConfig: IMongoOptions) {
    this.setConnectionString(mongoConfig)
    this.mongoOptions = mongoConfig.mongoOptions
    this.dbName = mongoConfig.mongoDbName
  }

  public async get(
    query: FilterQuery<T>,
    options?: FindOneOptions,
    orderField?: string | object | object[],
    direction?: number,
  ) {
    return (await this.getCursor(query, options, orderField, direction)).toArray()
  }

  public async getCursor(
    query: FilterQuery<T>,
    options?: FindOneOptions,
    orderField?: string | object | object[],
    direction?: number,
  ) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    if (!_.isEmpty(orderField)) {
      return await table.find(query, options).sort(orderField, direction)
    } else {
      return await table.find(query, options)
    }
  }

  public async insertOne(fullDoc: T, options?: CollectionInsertOneOptions) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    // A weird practice in the mongo that insertOne function change the input parameter (fullDoc)
    // makes the side-effects for those who wants to use fullDoc after insert, so cloning fullDoc
    // in order to keep the fullDoc unchanged
    const cloneDoc = _.cloneDeep(fullDoc)
    return await table.insertOne(cloneDoc as any, options)
  }

  public async insertOneBulk(fullDoc: T) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    // A weird practice in the mongo that insertOne function change the input parameter (fullDoc)
    // makes the side-effects for those who wants to use fullDoc after insert, so cloning fullDoc
    // in order to keep the fullDoc unchanged
    const cloneDoc = _.cloneDeep(fullDoc)
    await this.initBulkOps()
    return this.bulkOps.insert(cloneDoc)
  }

  public async updateOne(doc: T, query: FilterQuery<T>, options?: FindOneAndUpdateOption) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    const cloneDoc = _.cloneDeep(doc)
    return await table.findOneAndUpdate(
      query,
      { $set: cloneDoc },
      Object.assign({}, options, { returnOriginal: false }),
    )
  }

  public async updateOneBulk(doc: T, query: FilterQuery<T>) {
    await this.getDb()
    const cloneDoc = _.cloneDeep(doc)
    await this.initBulkOps()
    return this.bulkOps.find(query).updateOne(cloneDoc)
  }

  public async findOneAndDelete(query: FilterQuery<T>, options?: FindOneAndDeleteOption) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    return await table.findOneAndDelete(query, options)
  }

  public async initBulkOps() {
    if (this.bulkOps) {
      return
    }
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    this.bulkOps = await table.initializeOrderedBulkOp()
  }

  public async execute() {
    this.bulkOps.execute()
  }

  public async deleteOne(query: FilterQuery<T>, options?: CommonOptions & { bypassDocumentValidation?: boolean }) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    return await table.deleteOne(query, options)
  }

  public async deleteOneBulk(query: FilterQuery<T>) {
    await this.getDb()
    this.initBulkOps()
    return await this.bulkOps.find(query).deleteOne()
  }

  public async deleteMany(filter: FilterQuery<T>, options?: CommonOptions) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    return await table.deleteMany(filter, options)
  }

  public async aggregate<E>(match: { $match: T }, group: object, options?: CollectionAggregationOptions) {
    await this.getDb()
    const table = this.db.collection<T>(this.collectionName)
    return await table.aggregate<E>([match, group], options).toArray()
  }

  public async getDb() {
    if (_.isNil(MongoAccessor.mongoClient)) {
      MongoAccessor.mongoClient = await MongoClient.connect(this.mongoConnection, this.mongoOptions)
    }
    if (_.isNil(this.db)) {
      this.db = MongoAccessor.mongoClient.db(this.dbName)
    }
    return this.db
  }

  public async getCollection() {
    const db = await this.getDb()
    return db.collection<T>(this.collectionName)
  }

  private setConnectionString(mongoConfig: IMongoOptions) {
    if (mongoConfig.mongoUseDnsSeedlist) {
      this.mongoConnection =
        `mongodb+srv://${encodeURIComponent(mongoConfig.mongoUsername)}:` +
        `${encodeURIComponent(mongoConfig.mongoPassword)}@${mongoConfig.mongoHost}` +
        `/${mongoConfig.mongoDbName}${mongoConfig.mongoConnectOptions || ''}`
    } else {
      this.mongoConnection =
        `mongodb://${encodeURIComponent(mongoConfig.mongoUsername)}:` +
        `${encodeURIComponent(mongoConfig.mongoPassword)}@${mongoConfig.mongoHost}:` +
        `${mongoConfig.mongoPort}/${mongoConfig.mongoDbName}${mongoConfig.mongoConnectOptions || ''}`
    }
  }
}
