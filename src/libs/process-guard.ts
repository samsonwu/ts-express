import * as uuid from 'uuid/v4'

import { expressSvr } from '..'
import { Logger } from './logger'
import { logLevel, logTransports } from '../configs/index'

const processLogger = new Logger('my-app', uuid(), logLevel, logTransports)

const closeGracefully = async (event: string) => {
  await processLogger.info(`[Process] ${event} exit`)
  expressSvr.close(() => process.exit(0))
}

process
  .on('unhandledRejection', async (error: Error) => {
    await processLogger.error(error)
    await processLogger.info('[Process] unhandledRejection exit')
  })
  .on('uncaughtException', async (error: Error) => {
    processLogger.error(error)
  })
  .on('warning', async (warning) => {
    console.warn(warning.name)
    console.warn(warning.message)
    console.warn(warning.stack)
  })
  .on('SIGTERM', async () => await closeGracefully('SIGTERM'))
  .on('SIGINT', async () => await closeGracefully('SIGINT'))
