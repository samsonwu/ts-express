import { Response } from 'express'
import { ICustomRequest } from '../middlewares/create-logger'
import { Models } from '../models'
import { notFound } from '@hapi/boom'

export class UserController {
  static async get(req: ICustomRequest, res: Response) {
    const { params } = req
    const { id } = params
    const result = await Models.userModel.getUser(id)
    if (result.length > 0) {
      res.status(200).json(result[0])
    } else {
      throw notFound(`User Id [${id}] is not found.`)
    }
  }
}
