import { MongoClient } from 'mongodb'
import { mongoConfig } from '../../src/configs'

const initDBUser = async () => {
  // Since password was passed from outside not .env file
  // so we have to let ppl know they has to pass it explicitly in command.
  if (!mongoConfig.mongoPassword) {
    console.log('Mongodb password is empty, please pass it via [MONGO_CONFIG_PASSWORD=***], abort.')
    return
  }
  const connectionString =
    `mongodb://${encodeURIComponent(process.env.MONGO_INITDB_ROOT_USERNAME)}:` +
    `${encodeURIComponent(process.env.MONGO_INITDB_ROOT_PASSWORD)}@localhost:` +
    `${mongoConfig.mongoPort}/admin`

  const adminClient = await MongoClient.connect(connectionString, { useUnifiedTopology: true })
  const database = adminClient.db(mongoConfig.mongoDbName)
  // await database.dropDatabase();
  // await database.removeUser(mongoConfig.mongoUsername).catch(() => undefined);
  try {
    await database.addUser(mongoConfig.mongoUsername, mongoConfig.mongoPassword, {
      roles: [{ role: 'dbOwner', db: mongoConfig.mongoDbName }],
    })
    console.log(`Add user ${mongoConfig.mongoUsername} to ${mongoConfig.mongoDbName} success`)
  } catch (error) {
    console.log(`User ${mongoConfig.mongoUsername} has been add to ${mongoConfig.mongoDbName}`)
  } finally {
    await adminClient.close()
  }
}

;(async () => {
  await initDBUser()
})()
