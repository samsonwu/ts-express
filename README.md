# The TypeScript for Express.js

## What is this repository for?

### Quick summary

This is a repo to show how to write / run / debug and dockerize the TypeScript project in my previous experience based on Express.js.

## How do I get set up?

### Summary of set up

The recommended tools for running up the environment:

- [Visual studio code](https://code.visualstudio.com/), it is a good tool that making a balance between IDE and Text editor, a tool that easily extend and customize.
- VS Code extension - [ESLint](https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint) is to support eslint rules for the project
- VS Code extension - [Prettier](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode) Code formatter that support the eslint rules, it also format the code on save.

### Configuration

Required global packages:

The project only required yarn as the global package:

```sh
npm i -g yarn
```

For Visual studio code debugging, please copy `.vscode/launch.example.json` and rename to `.vscode/launch.json`, after this, you should see the options of debugging in vs code.

### Database configuration

You should initiate the business database before you use mongodb in docker, please run the following script in the root directory:

```sh
docker-compose up -d mongo
npx ts-node -r dotenv/config ./scripts/mongo/init-auth.ts dotenv_config_path=./environments/docker.env
```

## How to run tests

The following command is to run test in your local environment, docker mode is going to be enhance:

```sh
npm run test
```

The following command is to run the test coverage by nyc:

```sh
npm run coverage
```

### Deployment instructions

N/A

### Writing tests

The test is writing in mocha / sinon / chai, please check this [test](https://bitbucket.org/samsonwu/ts-express/src/master/src/middlewares/create-logger.test.ts) as a reference.

### Folder structure

Please check the folder structure of this project

    .
    ├── .vscode                 # default settings of vscode
    ├── environment             # Environment variables of this project (store your secret data somewhere else)
    ├── output                  # Compiled files from TypeScript to JavaScript
    ├── scripts                 # System scripts
    ├── src                     # All the source code of TypeScript
    │   ├── configs             # Application configs loads from environment variables
    │   ├── controllers
    │   ├── libs                # All the libraries use in the project, it can be separated into a npm package
    │   ├── middlewares         # The middlewares of express framework
    │   ├── models              # The data models of mongo database
    │   └── routes              # The system routes
    ├── .eslintrc               # All the eslint rules
    ├── pm2.json                # The process manager of node.js which helps manage multi instances of node.js
    └── tsconfig.json           # TypeScript configuration for back-end development
